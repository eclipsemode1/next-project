import React from 'react';
import Nav from "./Nav/Nav";
import Head from "next/head";

const MainContainer = ({children, keywords}) => {
    return (
        <>
            <Head>
                <meta keywords={'Next test project' + ' ' + keywords}></meta>
                <title>Next Project</title>
            </Head>
            <Nav/>
            <div>
                {children}
            </div>
        </>
    );
};

export default MainContainer;