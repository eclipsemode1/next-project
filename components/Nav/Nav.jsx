import React from 'react';
import A from "../UI/A/A";
import styles from './Nav.module.scss';

const Nav = () => {
    return (
        <nav className={styles.navbar}>
            <A text={'Main'} href={'/'}/>
            <A text={'Users'} href={'/users'}/>
        </nav>
    );
};

export default Nav;