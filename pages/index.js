import React from 'react';
import MainContainer from "../components/MainContainer";

const Index = () => {
    return (
        <MainContainer keywords={'Main page'}>
            <h1>Main Page</h1>
        </MainContainer>
    );
};

export default Index;