import { useRouter } from "next/router";
import MainContainer from "../../components/MainContainer";

function User( {user} ) {
    const { query } = useRouter();
    return (
        <MainContainer keywords={'User id: ' + user.id}>
                <h2>Пользователь c id {query.id}</h2>
                <div>Имя пользователя - {user.username}</div>
        </MainContainer>
    )
}

export default User;

export async function getServerSideProps(context) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/users/${context.params.id}`);
    const user = await response.json();
    return {
        props: { user },
    }
}